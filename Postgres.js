'use strict'
const pg = require('pg');

var pool = new pg.Pool({
    user: 'postgres', //env var: PGUSER
    database: 'my_db', //env var: PGDATABASE
    password: '123', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, 
    max: 10,
    idleTimeoutMillis: 1000,
});

const Postgres = {
	
	findAll: (table, order_field, cb) => {
		pool.connect();
		var query = pool.query("select * from " + table +" order by "+ order_field, (err, res) => {
			if(err) console.log(err);
			else cb(res.rows);
		});
	},
	find: (table, id_field, id, cb) => {
		pool.connect();
		var query = pool.query("select * from " + table +" where "+ id_field + " =$1", [id], (err, res) => {
			if(err) console.log(err);
			else cb(res.rows);
		});
	},
	update: (table, id_field, id, field, value, cb) => {
		pool.connect();
		var query = pool.query("update " + table +" set " + field + " = " + value + " where " + id_field+ " =$1",[id], (err, res) => {
			if(err) console.log(err);
			else cb(res.rows);
		});
	},
	create: (table, fields, values, cb) => {
		pool.connect();
		var query = pool.query("insert into " + table +"( " + fields.toString() + ") values ('" + values.toString()+"')", (err, res) => {
			if(err) {
				console.log(err);
			}
			else cb(res.rows);
		});
	},
	delete: (table, id_field, id, cb) => {
		pool.connect();
		var query = pool.query("delete from " + table +" where "+ id_field + " =$1", [id], (err, res) => {
			if(err) console.log(err);
			else cb(res.rows);
		});
	},
}
module.exports = Postgres;